﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class ProceduralGeometry : MonoBehaviour
{
    private Mesh mesh;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;

        UpdateMesh();
    }

    public void UpdateMesh()
    {
        List<Vector3> vertices = new List<Vector3>();

        vertices.Add(new Vector3(0, 3, 0));
        vertices.Add(new Vector3(-3, 5, 0));
        vertices.Add(new Vector3(0, 6, 0));

        mesh.SetVertices(vertices);

        int[] indices = new int[]
        {
            0, 1, 2
        };

        mesh.SetTriangles(indices, 0);

        List<Vector2> uvs = new List<Vector2>();

        uvs.Add(new Vector2(1, 0));
        uvs.Add(new Vector2(0, 0.5f));
        uvs.Add(new Vector2(1, 1));

        mesh.SetUVs(0, uvs);


        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
