﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorVisualizer : MonoBehaviour
{
    private Vector3 position = Vector3.zero;
    private Vector3 vector = Vector3.forward;
    private GameObject arrow;

    // Start is called before the first frame update
    void Start()
    {
        GameObject prefab = Resources.Load<GameObject>("Vector");
        arrow = Instantiate(prefab, position, Quaternion.identity);
        Set(position, vector);
    }
    
    public void Set(Vector3 newPosition, Vector3 newVector)
    {
        position = newPosition;
        vector = newVector;

        if (arrow == null)
            return;

        arrow.transform.position = position;
        if(vector.magnitude > 0.0001f)
        {
            arrow.transform.rotation = Quaternion.LookRotation(vector.normalized);

            Vector3 scale = arrow.transform.localScale;
            scale.z = -vector.magnitude;
            arrow.transform.localScale = scale;
        }
    }
}
