﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public struct HighScore
{
    public string name;

    public int score;

    public HighScore(string newName, int newScore)
    {
        name = newName;
        score = newScore;
    }
}

public class HighScoreManager : MonoBehaviour
{
    public List<HighScore> scores = new List<HighScore>();

    private void Awake()
    {
        Load();
    }

    public void Save()
    {
        Debug.Log("Persistent data path: " + Application.persistentDataPath);

        string saveFile = Application.persistentDataPath + "/highscores.txt";

        StreamWriter writer = new StreamWriter(saveFile);

        for(int i = 0; i < scores.Count; ++i)
        {
            writer.WriteLine(scores[i].name + "|" + scores[i].score);
        }

        writer.Close();
    }

    public void Load()
    {
        /*scores.Add(new HighScore("Jon", 10000));
        scores.Add(new HighScore("Not Jon", 5000));
        scores.Add(new HighScore("Bob", 1000));*/

        string saveFile = Application.persistentDataPath + "/highscores.txt";

        StreamReader reader = new StreamReader(saveFile);

        scores.Clear();

        while(reader.Peek() > -1)
        {
            string s = reader.ReadLine();
            if (s.Length == 0)
                continue;

            int index = s.LastIndexOf('|');
            if(index == -1)
            {
                // Malformed high score: ignore
                continue;
            }

            HighScore hs = new HighScore();
            hs.name = s.Substring(0, index);
            
            if(int.TryParse(s.Substring(index+1, s.Length - (index+1)), out hs.score))
            {
                scores.Add(hs);
            }
        }

        reader.Close();
    }
}
