﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 3f;

    public GameObject turret;
    public GameObject chassis;

    public float forceAmount = 20f;  // F = m*a
    public float speedLimit = 5f;

    private Rigidbody rb;
    private Shooter shooter;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        shooter = GetComponent<Shooter>();
    }

    // FixedUpdate is called every time the physics engine wants to make a step forward in time
    private void FixedUpdate()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        // Moving a dynamic body using forces

        // Note: Whenever you are using AddForce to apply a force, you should be doing it in FixedUpdate()
        if (rb.velocity.magnitude < speedLimit)
        {
            rb.AddForce(forceAmount * new Vector3(horiz, 0f, vert));
        }

        Vector3 facingDirection = rb.velocity;
        facingDirection.y = 0f;
        if (facingDirection.magnitude > 0.01f)
        {
            chassis.transform.rotation = Quaternion.LookRotation(facingDirection.normalized);
        }
        //chassis.transform.rotation = Quaternion.LookRotation(new Vector3(horiz, 0f, vert).normalized);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            shooter.Fire(turret.transform.forward);
            //rb.AddForce(10*Vector3.up, ForceMode.Impulse);
        }

        // Moving a static body by changing its position
        //transform.position += speed * new Vector3(horiz, 0f, vert) * Time.deltaTime;
        //transform.position += new Vector3(5,0,0);

        // Moving a dynamic body by setting its velocity
        //rb.velocity = speed * new Vector3(horiz, 0f, vert);



        float degrees = 0.0f;
        float turretHoriz = Input.GetAxis("TurretHorizontal");
        float turretVert = Input.GetAxis("TurretVertical");

        // Got: x, y
        // Want: theta
        degrees = Mathf.Atan2(turretVert, turretHoriz) * (180 / Mathf.PI);


        turret.transform.rotation = Quaternion.AngleAxis(degrees, new Vector3(0, 1f, 0));  // Vector3.up
        
    }
}
