﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            //Destroy(this);
            // TODO: Scoring that persists between scene changes
            LevelManager.instance.goal.GotCoin();

            Destroy(gameObject);
        }
    }
}
