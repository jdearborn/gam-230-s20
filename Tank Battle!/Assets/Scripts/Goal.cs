﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public string nextLevel = "YOU ARE A FOOL";
    public int coinsNeeded = 0;

    private void Start()
    {
        gameObject.SetActive(coinsNeeded == 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            // Do a scene transition
            SceneManager.LoadScene(nextLevel);
        }
    }

    public void GotCoin()
    {
        coinsNeeded--;
        if (coinsNeeded <= 0)
        {
            gameObject.SetActive(true);
        }
    }
}
