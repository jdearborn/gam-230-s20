﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public GameObject target;
    private Vector3 oldPosition;

    // Start is called before the first frame update
    void Start()
    {
        if(target != null)
        {
            oldPosition = target.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            Vector3 separation = target.transform.position - oldPosition;  // (destination - source) or (target - self) or (end - start)

            transform.position += separation;
            oldPosition = target.transform.position;
        }
    }
}
