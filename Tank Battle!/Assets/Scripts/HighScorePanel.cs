﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScorePanel : MonoBehaviour
{
    public GameObject entryPrefab;

    // Start is called before the first frame update
    void Start()
    {
        List<HighScore> scores = GameManager.instance.GetComponent<HighScoreManager>().scores;

        int entryHeight = 60;

        for(int i = 0; i < scores.Count; ++i)
        {
            GameObject go = Instantiate(entryPrefab, new Vector3(650, 300 + 183 - i * entryHeight, 0), Quaternion.identity, transform);
            HighScoreEntry entry = go.GetComponent<HighScoreEntry>();
            entry.nameText.text = scores[i].name;
            entry.scoreText.text = scores[i].score.ToString();
        }
    }
}
