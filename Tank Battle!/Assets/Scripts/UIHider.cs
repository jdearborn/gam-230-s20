﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHider : MonoBehaviour
{
    public bool hidden = true;
    private Vector3 position;
    public Transform hiddenPosition;
    public AnimationCurve curve;

    // Start is called before the first frame update
    void Start()
    {
        position = transform.position;
        if(hidden)
        {
            transform.position = hiddenPosition.position;
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (hidden)
                Show();
            else
                Hide();
        }
    }

    public void Show()
    {
        StartCoroutine(DoShow());
        hidden = false;
    }

    public void Hide()
    {
        StartCoroutine(DoHide());
        hidden = true;
    }

    IEnumerator DoShow()
    {
        float t = 0f;

        while (t < 1f)
        {
            transform.position = Vector3.Lerp(hiddenPosition.position, position, curve.Evaluate(t));
            t += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator DoHide()
    {
        float t = 1f;

        while (t > 0f)
        {
            transform.position = Vector3.Lerp(hiddenPosition.position, position, curve.Evaluate(t));
            t -= Time.deltaTime;
            yield return null;
        }
    }
}
