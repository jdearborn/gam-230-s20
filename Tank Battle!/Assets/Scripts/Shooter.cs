﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float speed = 5f;

    public GameObject muzzleObject;

    private float shotCooldownTimer = -1f;
    public float shotCooldown = 1f;

    public AudioClip[] fireSounds = new AudioClip[3];

    // Update is called once per frame
    void Update()
    {
        if(shotCooldownTimer > 0f)
        {
            shotCooldownTimer -= Time.deltaTime;
        }
    }

    public void Fire(Vector3 direction)
    {
        if (shotCooldownTimer <= 0f)
        {
            GameObject go = Instantiate(bulletPrefab, muzzleObject.transform.position, muzzleObject.transform.rotation);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = speed * direction;
            }

            AudioSource.PlayClipAtPoint(fireSounds[Random.Range(0, fireSounds.Length)], muzzleObject.transform.position);

            shotCooldownTimer = shotCooldown;
        }
    }
}
