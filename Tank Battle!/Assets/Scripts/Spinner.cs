﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float x;
    public float y;
    public float z;

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(x * Time.deltaTime, y * Time.deltaTime, z * Time.deltaTime, Space.World);

        transform.rotation = Quaternion.Euler(x * Time.deltaTime, y * Time.deltaTime, z * Time.deltaTime) * transform.rotation;

        //Quaternion.AngleAxis(30, new Vector3(2, 1, 3).normalized);
        //transform.rotation = Quaternion.LookRotation(...);

        // TODO: Move this into its own script
        /*float y2 = amplitude * Mathf.Sin(frequency * Time.time * (2* Mathf.PI));
        Vector3 motion = new Vector3(0f, y2, 0f);
        transform.position += motion * Time.deltaTime;*/
    }
}
