﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombClicker : MonoBehaviour
{
    public GameObject bomb;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("Clicked left mouse button");

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if(Physics.Raycast(ray, out hitInfo))
            {
                Instantiate(bomb, hitInfo.point + new Vector3(0, 5, 0), Quaternion.identity);
            }
            /*if(Physics.Raycast(new Vector3(0, 5, 0), Vector3.down, out hitInfo))
            {
                if(hitInfo.collider.CompareTag("Player"))
                {
                    Debug.Log("Ray hit the player!");
                }
            }*/
        }
    }
}
