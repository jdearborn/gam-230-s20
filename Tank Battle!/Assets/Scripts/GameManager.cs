﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;

    public static GameManager instance {
        get { return _instance; }
        /*set { _instance = value; }*/
    }


    public int score = 0;

    private void Awake()
    {
        // Singleton pattern:
        // 1) Global access to this object (instance)
        // 2) Only a single instance allowed
        if(_instance == null)
        {
            // We are the first!
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // Not first, so kill yourself...
            Destroy(gameObject);
        }
    }


}
