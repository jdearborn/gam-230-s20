﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform ammoStart;

    private GameObject[] ammoIcons;

    public static HUDManager instance = null;
    
    void Awake()
    {
        instance = this;

        ammoIcons = new GameObject[10];
        for(int i = 0; i < ammoIcons.Length; ++i)
        {
            Vector3 position = ammoStart.position + new Vector3(i, 0, 0);
            ammoIcons[i] = Instantiate(bulletPrefab, position, bulletPrefab.transform.rotation);
        }

        SetAmountOfAmmo(3);
    }

    public void SetAmountOfAmmo(int amount)
    {
        for (int i = 0; i < ammoIcons.Length; ++i)
        {
            ammoIcons[i].SetActive(i < amount);
        }
    }
}
