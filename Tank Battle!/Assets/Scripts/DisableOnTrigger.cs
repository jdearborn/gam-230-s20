﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // Disable the game object this script is attached to
        gameObject.SetActive(false);
        
        // Use this to check if a game object is disabled or enabled
        //gameObject.activeSelf
    }
}
