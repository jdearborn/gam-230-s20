﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorTester : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        VectorVisualizer vis = gameObject.AddComponent<VectorVisualizer>();

        Vector3 a = new Vector3(0, 0, 1);
        Vector3 b = new Vector3(5, 6, 7);

        vis.Set(Vector3.zero, a * Vector3.Dot(a, b));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
