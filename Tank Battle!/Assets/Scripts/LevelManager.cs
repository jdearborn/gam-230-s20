﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Goal goal;
    public GameObject player;

    public static LevelManager instance;

    // Called immediately when this object is created
    private void Awake()
    {
        instance = this;
    }

    // Called before the first frame
    private void Start()
    {
    }
}
