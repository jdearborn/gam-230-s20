﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawning(3f, 5f));
    }

    void SpawnObject()
    {
        Instantiate(enemyPrefab, transform.position, transform.rotation);
    }

    IEnumerator StartSpawning(float delay, float interval)
    {
        yield return new WaitForSeconds(delay);

        SpawnObject();

        while (true)
        {
            yield return new WaitForSeconds(interval);
            SpawnObject();
        }
    }
}
