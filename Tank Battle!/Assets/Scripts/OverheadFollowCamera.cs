﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverheadFollowCamera : MonoBehaviour
{
    public GameObject target;

    // LateUpdate is called once per frame, after all Updates have been processed.
    void LateUpdate()
    {
        transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
    }
}
