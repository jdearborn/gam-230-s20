﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTrigger : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("DestroyOnTrigger object created.");
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("This thing was trigggggered!");
        Destroy(gameObject);
    }
}
