﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MoveToPlayer : MonoBehaviour
{
    private NavMeshAgent agent;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.destination = LevelManager.instance.player.transform.position;

        if (rb != null)
        {
            rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
        }
    }
}
